

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <inttypes.h>
#include <sys/types.h>
#include <netdb.h>



#include "constants.h"    // pouze konstanty - SEQ_NUMBER_SIZE 2, MESSAGE_SIZE 200, TIMEOUT 1000, WINDOW_SIZE 8, PROBABILITY 90
#include "window.h"	     // 
#include "util.h"

int udp_server_socket(const char *host, const char *port) {
    struct addrinfo hints;
    struct addrinfo *result, *r;
    int e;

    memset(&hints, 0, sizeof(hints));      // na prvních sizeof(hints) charakterů HInts dá nuly (pravdepodobne predinicializace všech hodnot)
    hints.ai_family = AF_UNSPEC;  			/* allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_protocol = 0;			// any protocol			//TO SAMY NAJDETE V man addinfo
    hints.ai_canonname = NULL;   // must be 0 or null
    hints.ai_addr = NULL;	// must be 0 or null
    hints.ai_next = NULL;	// must be 0 or null

    e = getaddrinfo(host, port, &hints, &result);		// kontrola, if -1 tak error
    if (e != 0) {						// do Result dá pointer na začátek Hints (jsou to linked listy)
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(e));	// hints a results
        exit(EXIT_FAILURE);
    }

	      /* getaddrinfo() returns a list of address structures.
              Try each address until we successfully bind(2).
              If socket(2) (or bind(2)) fails, we (close the socket
              and) try the next address. */

	
		//  The items in the linked list are linked by the ai_next field.	
    for (r = result; r != NULL; r = r->ai_next) {		// tenhle cyklus vyzkouší vsechny adressy z toho listu, dokud uspesne nenabinduje
        int s = socket(r->ai_family, r->ai_socktype, r->ai_protocol);     // pokusi se vytvorit end-point komunikace a vrátí na něj file descr.
        if (s == -1) {    
            continue;    // tahle adresa selhala, ukoncuju iteraci a jdu na dalsi, socket se nepodarilo vytvorit
        }

        if (bind(s, r->ai_addr, r->ai_addrlen) != 0) {     // "bind == assigning a name to a socket"
            close(s);					   //přiřadí adresu "ai_addr" tomu socketu, na kterej odkazuje file descriptor "s"
            continue;					   // ai_Addrlen je velikost ty address strukury v bytech
        }			// kdyz neni 0 (je -1), tak zavíram ten socket (sice ta adresa neselhala, ale neda se na ni pripojit
				// a pokracuju na dalsi iteraci (na dalsi adresu v tom linked listu)

        freeaddrinfo(result);    //v pripade ze se pripojim na adresu, tohle uz nepotrebuju a vracim file descriptor na ten endpoint komunikace	
        return s;
    }

    freeaddrinfo(result);     // v pripade ze to celý failne, nenajde se adresa na kterou by se to pripojilo
    fprintf(stderr, "Failed\n");
    exit(EXIT_FAILURE);
}

//void write_seq2(char first, char second)

void run_server(int listenSocket, int verbosity) {
    int waitingForSeq = 0;

    Window window;  // vytvori vokno 
    init_window(&window);		// nastavi jeho seq[i] na -1

    for (;;) {
        char buf[SEQ_NUMBER_SIZE + MESSAGE_SIZE + 2];    // delka 203, pravdepodobne dva byty + 200 msg lenght + "\0" ukončovací char
        int seq;
        ssize_t n;
        struct sockaddr_storage addr;       		// sockadd_storage neresi ip4 nebo ipv6 nebo jak to je
        socklen_t addrLen = sizeof(addr);		// delka adresy v promenny

        /* receive frame */
        n = recvfrom(listenSocket, buf, sizeof(buf), 0, (struct sockaddr*) &addr, &addrLen); // recieves massages fom socket
			// parametry: fd na socket, kam to ulozim, velikost ukladaciho bufu, flags (see MAN recvfrom) addresa, velikost adresy)
        
        if (n < SEQ_NUMBER_SIZE) {     
            fprintf(stderr, "Frame is too short\n");
            continue;
        }								// NESPRAVNE DELKY SEKVENCI
        if (n > sizeof(buf)) {
            fprintf(stderr, "Frame is too long\n");
            continue;
        }

        seq = read_seq(buf);   
	
	
	int delkaZpravy = (unsigned char) buf[2];
	printf("delka prijat zpravy je: %d \n", delkaZpravy);
	char message[delkaZpravy];
	
	memcpy(message, buf +3, delkaZpravy);
	
	uint8_t checksum = count_checksum(message, delkaZpravy);  // prijatej checksum

	if(checksum_message_equation(checksum, buf[n-1]) == 0){		
		continue;
	}

        if (seq == -1) {
            fprintf(stderr, "Invalid seq\n");  // detekce chybnho cteni
            continue;
        }
        if (verbosity > 0) {
            fprintf(stderr, "Got frame with seq %d\n", seq);    
        }
	/* seq >= waitingForSeq */        /* seq < waitingForSeq + WINDOW_SIZE */
        if (seq_ge(seq, waitingForSeq)  &&  seq_lt(seq, waitingForSeq + WINDOW_SIZE)) {
            if (! window_has_seq(&window, seq)) {  
                if (verbosity > 0) {
                    fprintf(stderr, "Saving seq %d to the window\n", seq);
                }
                window_store(&window, seq, buf+SEQ_NUMBER_SIZE+1, n-SEQ_NUMBER_SIZE-2, checksum);
            }
        }

        while (window_has_seq(&window, waitingForSeq)) {
            window_print_message(&window, waitingForSeq, stdout);
            if (window_get_message_length(&window, waitingForSeq) == 0) {
                fclose(stdout);
            }
            waitingForSeq = seq_inc(waitingForSeq);
        }

        fflush(stdout);
	

        /* send ACK */
        {
            char ackBuf[SEQ_NUMBER_SIZE];    // dva byty
  
            write_seq(ackBuf, waitingForSeq);    // read_seq udela z prvnich dvou znaku zpravy 16bitovy sekvencni cislo, write_Seq z nej udela
							// zase zpatky znaky

            if (random_number() < PROBABILITY) {   // pokud padne pravdepodobnost
		// posilam ackbuf (dvoubytova sekvence) na socket
            	n = sendto(listenSocket, ackBuf, sizeof(ackBuf), 0, (struct sockaddr*) &addr, addrLen); 
		
            }
        }
    }
}


struct ServerOptions {
    const char *host;
    const char *port;
    int verbosity;
};


static void process_arguments(int argc, char *argv[], struct ServerOptions *);


int main(int argc, char *argv[]) {    
    int s;
    struct ServerOptions options;
    memset(&options, 0, sizeof(options));       // naplní options nulama 
    options.host = NULL;    			// prednastaví hodnoty na null / 0
    options.port = NULL;
    options.verbosity = 0;

    process_arguments(argc, argv, &options);   // defakto jen rozebere prepinace a ulozi port do options.port

    s = udp_server_socket(options.host, options.port);    // nastavi server, metoda vraci file descriptor na vytvorenej socket
    run_server(s, options.verbosity);			// spusti server, posílá metodě fd na socket a verbosity (mela by byt 0)

    return 0;
}


static void print_usage(const char *programName, FILE *out) {    // jen tiskne usage
    fprintf(out, "Usage: %s [<options>] <port>\n", programName);
}


static void print_help(const char *programName) {
    print_usage(programName, stdout);				// jen tiskne help
    printf("\n");
    printf("Options:\n");
    printf("  -h            Show this help\n");
    printf("  -b <address>  Bind to a specific address\n");
    printf("  -v            Be more verbose; can be used multiple times\n");
}


static void process_arguments(int argc, char *argv[], struct ServerOptions *options){  // rozebere argumenty, -v navysuje verbosity variable
										 // -b binduje na specifickej host (defaultne je u serveru host
    int ch;
    while ((ch = getopt(argc, argv, "hb:v")) != -1) {    // :v - ta dvojtecka znamena, ze to "v" se muze opakovat
	
        switch (ch) {
            case 'h':
                print_help(argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case 'b':
                options->host = optarg;
                break;
            case 'v':
                options->verbosity++;
                break;
            case '?':
            default:
                print_usage(argv[0], stderr);
                fprintf(stderr, "Run %s -h for help.\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (optind != argc - 1) {
        fprintf(stderr, "Port number must be provided as argument.\n");
        print_usage(argv[0], stderr);
        exit(EXIT_FAILURE);
    }

    options->port = argv[optind];   // ulozi port do options.port
}


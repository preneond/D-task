#ifndef CONSTANTS_H
#define CONSTANTS_H


#define SEQ_NUMBER_SIZE 2
#define MESSAGE_SIZE 255
#define TIMEOUT 1

#define WINDOW_SIZE 8
#define PROBABILITY 90

#define max(x, y) (((x) > (y)) ? (x) : (y))

#define SEQMOD 0x10000
#define HALFSEQ 0x8000

#endif 



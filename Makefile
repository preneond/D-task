# report more warnings
CFLAGS=-Wall -pedantic

# default target
all: client server

check: client server
	./run_tests.py

# target for removing all temporary files
clean:
	rm -vf client server *.o

# programs to build
client: client.o window.o util.o

server: server.o window.o util.o

# dependencies to header files
client.o: util.h

server.o: constants.h window.h util.h



window.o: window.h constants.h

util.o: util.h


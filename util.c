
#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include "constants.h"  



uint8_t count_checksum(char*buff, int length){
	int i;
	uint8_t chcksum = (uint8_t) buff[0];
	//printf("znak je: %c \n", buff[0]);	
	for(i=1;i<length-1;i++){          		// nebere v potaz \0 - lepe se mi ověřovalo
		//printf("znak je: %c \n", buff[i]);
		chcksum ^= buff[i];
	}
	return chcksum;
}	

int checksum_message_equation(uint8_t checksum, uint8_t checksum_orig_message){
	//printf("checksum origo: %d  a prijatej %d\n", checksum_orig_message, checksum);
	if(checksum == checksum_orig_message){
		return 1;
	}
	return 0;
}


/* I want the seq numbers to be human readable and writable, for easy testing */
int read_seq(const char *data) { 	// data je message prectena ze socketu, len 203
    int i, seq = 0;
    for (i = 0; i < SEQ_NUMBER_SIZE; i++) {   // probehne 2x
    				
				
        seq <<= 8;					// po prvni iteraci se nic nemeni, na prvnich 8 bitu seq ulozim bity data[i]
        seq += (unsigned char) data[i];			// shiftnu doleva a ulozim dalsich 8 bitu
    						// tzn seq by mohlo vypadat neco jako 1000010010101001   ( např)
    }							// unsigned je 0..255, char je defaultně signed, tzn -127..128 či co
    return seq;
}


void write_seq(char *buffer, int seq) {     // buffer =tam ukladam, seq = waitingforseq, JESTLI spravne chapu, tak to zase dekoduje tu sekvenci
    int i;				    
    for (i = SEQ_NUMBER_SIZE - 1; i >= 0; i--) {     // proběhne 2x
    
    
        buffer[i] = seq & 0xFF;   // 0xFF je 0...000011111111 (číslo 255 zapsano binarne), a "něco & 0xFF" nechá teda jen 8 poslednich (prvnich)
											   // bitů toho čísla "něco", zbytek dá na nuly 
	// tzn do buffer[1] se ulozi poslednich (1,2,4,8...prvních? nevim) 8 bitů seq
        seq >>= 8;  // shiftnu doprava, tzn ty prvni bity seq zahodim a vysledek ulozim opet do seq
		// tim padem do buffer[0] se ulozi tech 8 vetsich bitů původního seq, takze nejakej char co má v ascii kod z intervalu 0..255
    
    }
}



   



int seq_lt(int a, int b) {    // less than,  u server to volam s seq < waitingForSeq + WINDOW_SIZE

    int d = a - b;		
    if ((d < 0 && d > -HALFSEQ) || (d > HALFSEQ)) {
        return 1;
    } else {
        return 0;
    }
}

int seq_gt(int a, int b) {     // greater than   , u serveru to volam s seq, waitingForSeq 
    int d = a - b;		// takze vetsinou d = seq - waitingForSeq, coz by melo bejt ve vetsine pripadu kladný a zaroven menší než 3k
    if ((d > 0 && d < HALFSEQ) || (d < -HALFSEQ)) {   
        return 1;
    } else {
        return 0;
    }
}

int seq_ge(int a, int b) {   // greater or equal, u serveru to volam s seq, waitingForSeq    
    if (a == b) {     
        return 1;
    }
    return seq_gt(a, b);
}

int seq_inc(int seq) {         // seq+1 % 65536
    return (seq + 1) % SEQMOD;
}

int random_number() {    // generates random number 0;100
    int r = rand() % 100;
    return r;
}


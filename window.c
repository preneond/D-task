#include "window.h"

#include <string.h>
#include <inttypes.h>

void init_window(Window *window) {    // nastavi "seq" hodnoty pole WindowItemů (delka 8) na -1
    int i;
    for (i = 0; i < WINDOW_SIZE; i++) {   	// tahle metoda se spusti jako prvni vzdy
        (*window)[i].seq = -1;
    }
}

		
int window_has_seq(Window *window, int seq) {        // asi jen kontrola jestli v tom okně uz je nějaka .seq
    if ((*window)[seq % WINDOW_SIZE].seq == seq) {	
        return 1;					
    } else {
        return 0;
    }
}


void window_store(Window *window, int seq, char *message, int messageLength, uint8_t checksum) {    
    struct WindowItem *wi = &((*window)[seq % WINDOW_SIZE]);           
    wi->seq = seq;          				
    wi->length = messageLength;
    memcpy(wi->message, message, messageLength);
    wi->checksum = checksum;     
}


void window_print_message(Window *window, int seq, FILE *out) {
    struct WindowItem *wi = &((*window)[seq % WINDOW_SIZE]);
    if (wi->seq != seq) {
        return;
    }
    fwrite(wi->message, wi->length, 1, out);
}


char* window_get_message(Window *window, int seq) {
    struct WindowItem *wi = &((*window)[seq % WINDOW_SIZE]);
    if (wi->seq != seq) {
        return NULL;
    }
    return wi->message;
}

unsigned char window_get_message_length(Window *window, int seq) {
    struct WindowItem *wi = &((*window)[seq % WINDOW_SIZE]);
    if (wi->seq != seq) {
        return 0;
    }
    return wi->length;
}

uint8_t window_get_checksum(Window *window, int seq) {
    struct WindowItem *wi = &((*window)[seq % WINDOW_SIZE]);
    if (wi->seq != seq) {
        return 0;
    }
    return wi->checksum;
}




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "constants.h"
#include "window.h"
#include "util.h"
#include <inttypes.h>



int udp_client_socket(const char *host, const char *port) {
    struct addrinfo hints;
    struct addrinfo *result, *r;
    int e;

    memset(&hints, 0, sizeof(hints));	
    hints.ai_family = AF_UNSPEC;  /* allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    e = getaddrinfo(host, port, &hints, &result);
    if (e != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(e));
        exit(EXIT_FAILURE);
    }

    for (r = result; r != NULL; r = r->ai_next) {
        int s = socket(r->ai_family, r->ai_socktype, r->ai_protocol);    // pokusi se vytvorit end-point komunikace a vrátí na něj file descr.
        if (s == -1) {							
            continue;   // kdyz fail, tak zkousim dalsi adresu v tom linked listu
        }							// AZ SEM UPLNE TO SAMY JAKO U SERVERU


	// připojí ten socket, na kterej ukazuje "s" (file descriptor, kterej vraci call socket() na adresu "Addr"
        if (connect(s, r->ai_addr, r->ai_addrlen) != 0) { 	// "addrlen" opet velikost addr v bytech
            close(s);				// v pripade se selhani, socket se vytvoril ale na danou adresu se neda pripojit
            continue;
        }

        freeaddrinfo(result);   // kdyz se na adresu pripojim, tak tohle uz nepotrebuju a vracim file descriptor na end-point komunikace
        return s;
    }

    freeaddrinfo(result);		// no more needed
    fprintf(stderr, "Failed\n");    // v pripade ze to CELY FAILNE KUNDO
    exit(EXIT_FAILURE);
}

static void print_usage(const char *programName, FILE *out) {           // vypise usage
    fprintf(out, "Usage: %s [<options>] <host> <port>\n", programName);
}


static void print_help(const char *programName) {			// vypise napovedu
    print_usage(programName, stdout);
    printf("\n");
    printf("Options:\n");
    printf("  -h  Show this help\n");
}


static void process_arguments(int argc, char *argv[], const char **host, const char **port) {    // prebere prepinace
    int ch;
    while ((ch = getopt(argc, argv, "h")) != -1) {
        switch (ch) {
            case 'h':
                print_help(argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case '?':
            default:
                print_usage(argv[0], stderr);
                fprintf(stderr, "Run %s -h for help.\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (optind != argc	 - 2) {
        fprintf(stderr, "Address and port number must be provided as arguments.\n");
        print_usage(argv[0], stderr);
        exit(EXIT_FAILURE);
    }

    *host = argv[optind];     // host
    *port = argv[optind+1];    // port
}




	


static void send_frame(int s, Window *window, int seq) {

    int n;
    char buf[SEQ_NUMBER_SIZE + window_get_message_length(window, seq) + 2];   

 	printf("Delka odesilane zpravy je: %d \n", SEQ_NUMBER_SIZE + window_get_message_length(window, seq) + 2);
    if (!window_has_seq(window, seq)) {
        return;
    }

    write_seq(buf, seq);
	
	char delka[1];
	delka[0] = window_get_message_length(window, seq);
	char cksum[1];
	cksum[0] = window_get_checksum(window, seq);

    memcpy(buf+SEQ_NUMBER_SIZE, ( char*) delka,  1);   

    memcpy(buf+SEQ_NUMBER_SIZE +1, (char*) window_get_message(window, seq), window_get_message_length(window, seq));
	
    memcpy(buf + SEQ_NUMBER_SIZE +1 +  window_get_message_length(window, seq), (char*) cksum, 1);
	
    if (random_number() < PROBABILITY) {
	int byteChangingProbability = 90;
	if(random_number() > byteChangingProbability){
		int random_byte_index = (rand() % (window_get_message_length(window, seq) -1)) +3;
		buf[random_byte_index]++;
		printf("zmenenej znak\n");	
		
	}		
		

        n = write(s, buf, SEQ_NUMBER_SIZE + window_get_message_length(window, seq) +2);		
        if (n == -1) {      // kontrola
            perror("write");
            exit(EXIT_FAILURE);
        }
    }else{
	printf("zprava se vubec neposle\n");
	}
}


void run_client(int s) {
    int serverWaitingForSeq = 0;
    int seqToFill = 0;
    int stdinClosed = 0;
    struct timeval lastTime;

    Window window;
    init_window(&window);

    for (;;) {
        int n;
        int maxFd = 0;
        fd_set rdset, wrset;    //vytvorim rd,wr set
        struct timeval selectTimeout;
        FD_ZERO(&rdset);	//vycistim rd set a wr set
        FD_ZERO(&wrset);	

        /* budeme cekat na data ze site */
        FD_SET(s, &rdset);			//cekam kdy mi ze file descriptoru na socket (ze socketu) přijdou data
        maxFd = max(maxFd, s);     //maxFD = je nejvetsi cislo file descriptoru (pozdeji pak k nemu pricitam jednicku, SEE man select)

        /* cekame na standardni vstup, pokud neni okno pro odeslani plne a vstup neni zavreny*/
        if (!stdinClosed && seq_lt(seqToFill, serverWaitingForSeq + WINDOW_SIZE)) {
            FD_SET(0, &rdset);
            maxFd = max(maxFd, 0);
        }

        selectTimeout.tv_sec = TIMEOUT;   // 1 sekunda, 0 milisekund
        selectTimeout.tv_usec = 0;	// timeout mi resi, po jaky době bude metoda select vracet, tady vraci kazdou sec
        n = select(maxFd+1, &rdset, &wrset, NULL, &selectTimeout);   // metoda nam umoznuje monitorovat vic file descriptoru do ty doby, dokud
				// neuplyne timeout a nebo nejakej z tech descriptorů je "ready", tzn ready provyst nejakou I/O operaci
				// to v nasem pripade resi ten algoritmus - do nejaky doby se musí vrátit potvrzená zpráva, jinak to klient 
				// vysílá znova

        if (n == -1) {
            perror("select");
            exit(EXIT_FAILURE);		// detekce chyby
        }

        if (FD_ISSET(0, &rdset)) {     // kdyz prisly nejaky data (zprava) (not sure jestli to znamena tohle, correct me)
           
            char buf[MESSAGE_SIZE];
            int n = read(0, buf, sizeof(buf));   // precteme data ze standartniho vstupu a posleme je
            if (n == -1) {
                perror("read");
                exit(EXIT_FAILURE);   // chyba cteni
            }

            if (n == 0) {
                stdinClosed = 1;     // zavru vstup
            }
	    uint8_t chcksum = count_checksum(buf, n);
	    //printf("pocet znaku je: %d \n", n);
            window_store(&window, seqToFill, buf, n, chcksum);     //dam to do okna
		//printf("dojdu sem?\n");
            send_frame(s, &window, seqToFill);		// pošlu to
		//printf("sem za tim\n");
            gettimeofday(&lastTime, NULL);		//
            seqToFill = seq_inc(seqToFill);		// seqtofill = seqtofill +1 % 65536
        }

        if (FD_ISSET(s, &rdset)) {    
            /* prislo neco po siti (zrejme potvrzeni) - prijmeme to (recv, read...) */
            /* zvednout serverWaitingForSeq, pokud cislo, ktere prijde, bude vyssi */
            /*  + odeslani prvniho ramce v okne */

            char buf[SEQ_NUMBER_SIZE];
            int n = read(s, buf, sizeof(buf));   // prectu to co prislo ze socket descriptoru (asi to potvrzeni)
            if (n == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            }
            if (n == SEQ_NUMBER_SIZE) {		
                /* je to datagram spravne delky */
                int seq = read_seq(buf);
                if (seq_gt(seq, serverWaitingForSeq) /* seq > serverWaitingForSeq */) {
                    while (seq_gt(seq, serverWaitingForSeq)) {
                        serverWaitingForSeq = seq_inc(serverWaitingForSeq);
                    }
                    if (stdinClosed && serverWaitingForSeq == seqToFill) {
                        /* transfer finished successfully */
                        break;
                    }
                } else if (seq == serverWaitingForSeq) {
                    /* server potvrzuje neco, co uz potvrdil; posleme znovu prvni
                       ramec z okna */
                    send_frame(s, &window, serverWaitingForSeq);
                    gettimeofday(&lastTime, NULL);
                }
            }
        }

        if (serverWaitingForSeq != seqToFill) {  /* je neco k odeslani */
            struct timeval now;
            int delta;
            gettimeofday(&now, NULL);
            delta = (now.tv_sec - lastTime.tv_sec) * 1000; /* in miliseconds */
            delta += (now.tv_usec - lastTime.tv_usec) / 1000;
            if (delta > TIMEOUT) {
                send_frame(s, &window, serverWaitingForSeq);
                gettimeofday(&lastTime, NULL);
            }
        }

    }
}


int main(int argc, char *argv[]) {
    const char *host = NULL;
    const char *port = NULL;
    int s;

    process_arguments(argc, argv, &host, &port);   // preberu argumenty a nastavim host a port

    fprintf(stderr, "Connecting to %s %s...\n", host, port);  // info o pripojeni

    s = udp_client_socket(host, port);    // vytvorim socket, s = file descriptor na ten socket

    run_client(s);			// spoustim klienta s fd na socket
    exit(EXIT_SUCCESS);
}


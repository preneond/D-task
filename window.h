
#ifndef WINDOW_H
#define WINDOW_H

#include "constants.h"   

#include <stdio.h>
#include <inttypes.h>


struct WindowItem {
    int seq;
    unsigned char length;
    char message[MESSAGE_SIZE];
    uint8_t checksum;
};

typedef struct WindowItem Window[WINDOW_SIZE]; 


extern void init_window(Window *window);

extern int window_has_seq(Window *window, int seq);

extern void window_store(Window *window, int seq, char *message, int messageLength, uint8_t checksum);

extern void window_print_message(Window *window, int seq, FILE *out);

extern char* window_get_message(Window *window, int seq);

extern unsigned char window_get_message_length(Window *window, int seq);

extern uint8_t window_get_checksum(Window *window, int seq);

#endif  /* WINDOW_H */


